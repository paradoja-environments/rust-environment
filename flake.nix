{
  description = "Rust dev environment";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShell = let inherit (nixpkgs.lib) optional optionals;

        in pkgs.mkShell {
          buildInputs = with pkgs; [
            rustc
            rust.packages.stable.rustPlatform.rustcSrc # rust-src
            cargo
            gcc
            rustfmt
            clippy
            rust-analyzer
            pkg-config

            # other build tools
            openssl

            # nix and others
            nixfmt
          ];
          RUST_SRC_PATH = pkgs.rust.packages.stable.rustPlatform.rustLibSrc;
          shellHook = "";
        };
      });
}
